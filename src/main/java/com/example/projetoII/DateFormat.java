package com.example.projetoII;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Calendar;
import java.util.Date;

public class DateFormat  {

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date data;

    @DateTimeFormat(pattern = "HH:mm")
    private Date hora;




    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }


    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }
}
