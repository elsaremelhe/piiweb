package com.example.projetoII.controller;

import com.example.projetoII.DateFormat;
import org.ipvcClinica.Conta;
import org.ipvcClinica.Procedimento;
import org.ipvcClinica.Utente;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class UserController {

    @GetMapping
    public String mainPage(Model model) {
        model.addAttribute("mensagem", "Página Inicial");

        return "users";
    }


    /**
     * Realiza a autenticação do utilizador.
     * @param formData
     * @param model
     * @return
     * */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String checkLogin(@ModelAttribute("userFormData") Conta formData, Model model) {


        System.out.println("Login...");
        String email = formData.getEmail();
        String password = formData.getPassword();

        System.out.println(email);
        System.out.println(password);

        Utente utente = null;

        Conta con = null;

        if (con.verificaEmail(email)){

            if (con.getContaPorCredenciais(email).getPassword().equals(password) && con.getContaPorCredenciais(email).getTipo().equals("utente")) {


                /**Obtém o utente à qual a conta pertence*/
                utente = Utente.getUtentePorConta(con.getContaPorCredenciais(email).getIdconta());

                System.out.println("Login com sucesso!");


                return "redirect:/perfilutilizador/" + utente.getIdutente();
            } else {

                model.addAttribute("error","Introduziu os dados errados.");

                return "users";
            }

        } else {


            model.addAttribute("error","Esse utilizador não existe.");
            return "users";
        }

    }

    /**
     * Redireciona o utilizador para a página de registo.
     * @return
     * */
    @RequestMapping(value="/registo")
    public String pagRegisto(){

        return "registo";

    }

    /**
     * Regista um novo utilizador.
     * @param formData
     * @param formData2
     * @param model
     * @param redirAttrs
     * @param dateFormat
     * @return
     * */
    @RequestMapping(value = "/registar", method = RequestMethod.POST)
    public String registarUtente(@ModelAttribute("userFormData") Conta formData, Utente formData2, DateFormat dateFormat, Model model, RedirectAttributes redirAttrs) {


        System.out.println("Registo...");

        String nome = formData2.getNome();


        Date data = dateFormat.getData();

        LocalDate datanasc=convertToLocalDateViaMilisecond(data);


        String contacto = formData2.getContacto();
        String nif = formData2.getNif();

        String morada= formData2.getMorada();

        String numcc = formData2.getNumcc();

        String email = formData.getEmail();
        String password = formData.getPassword();


        if (Conta.verificaEmail(email)){
            model.addAttribute("error","Já se encontra registado(a)!");
            return "registo";
        } else {
            Conta.addConta(email,password,"utente");

            Conta novoUtente = Conta.getContaPorCredenciais(email);

            Utente.addUtente(nome,datanasc,contacto,morada,nif,numcc,"",novoUtente.getIdconta());

            redirAttrs.addFlashAttribute("success","Registado com sucesso!");

            return "redirect:";
        }

    }

    /**
     * Função auxiliar para converter um Date em LocalDate.
     * @param dateToConvert
     * @return
     * */
    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }


    /**
     * Obtém a lista de todos os procedimentos disponiveis.
     * @param model
     * @return
     * */
    @RequestMapping(value="/procedimentos")
    public String pagProcedimentos(Model model){

        List<Procedimento> procedimentos = Procedimento.getListaProcedimentos();

        model.addAttribute("listaproced",procedimentos);

        return "procedimentos";

    }



}
