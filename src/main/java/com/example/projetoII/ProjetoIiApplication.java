package com.example.projetoII;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoIiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoIiApplication.class, args);
	}

}
