package com.example.projetoII.controller;

import com.example.projetoII.DateFormat;
import org.checkerframework.checker.units.qual.A;
import org.ipvcClinica.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class PerfilController {


    /**Lista de todas as consultas no sistema.*/
    List<Consulta> listaConsultas = Consulta.getListaConsultas();

    /**Lista de todas as consultas do utente.*/
    List<Consulta> listaPorUtente;

    @RequestMapping("/perfilutilizador/{id}")
    public String main(@PathVariable("id") int id, Model model) {

        model.addAttribute("idutilizador", id);

        listaPorUtente = getConsultasUtente(listaConsultas,id);

        model.addAttribute("listaconsultas", listaPorUtente);


        String methodMapping = new Object(){}.getClass().getEnclosingMethod().getAnnotation(RequestMapping.class).value()[0];
        System.out.println(methodMapping);

        return "perfilutilizador";


    }

    /**
     * Obtém a lista das consultas confirmadas de determinado utente.
     * @param id
     * @param model
     * @return
     * */
    @RequestMapping("/perfilutilizador/{id}/confirmadas")
    public String consultasConfirmadas(@PathVariable("id") int id, Model model) {

        model.addAttribute("idutilizador", id);

        listaPorUtente = getConsultasUtente(listaConsultas,id);

        List<Consulta> listaConsultasConfirmadas = getConsultasUtenteConfirmadas(listaPorUtente);


        model.addAttribute("listaconsultas", listaConsultasConfirmadas);

        return "perfilutilizador";
    }

    @RequestMapping("/perfilutilizador/{id}/hoje")
    public String consultasHoje(@PathVariable("id") int id, Model model) {

        model.addAttribute("idutilizador", id);

        listaPorUtente = getConsultasUtente(listaConsultas,id);

        List<Consulta> listaConsultasHoje = getConsultasUtenteHoje(listaPorUtente);


        model.addAttribute("listaconsultas", listaConsultasHoje);

        return "perfilutilizador";
    }

    @RequestMapping("/perfilutilizador/{id}/historico")
    public String consultasHistorico(@PathVariable("id") int id, Model model) {

        model.addAttribute("idutilizador", id);

        listaPorUtente = getConsultasUtente(listaConsultas,id);

        List<Consulta> listaConsultasHistorico = getConsultasUtenteHistorico(listaPorUtente);

        model.addAttribute("listaconsultas", listaConsultasHistorico);

        return "perfilutilizador";
    }


    /**
     * Redireciona o utilizador para a pagina da consulta(Ver detalhes consultas)
     * @param model
     * @param idconsulta
     * @return
     */
    @RequestMapping("/perfilutilizador/consulta/{idconsulta}")
    public String verConsulta(@PathVariable("idconsulta") int idconsulta, Model model) {
        model.addAttribute("mensagem", "ID Consulta" + idconsulta);

        Consulta c = Consulta.getConsulta(idconsulta);

        model.addAttribute("consulta", c);

        return "consulta";
    }

    /**
     * Redireciona o utilizador para a pagina da receita
     * @param idconsulta
     * @param model
     * @return
     */
    @RequestMapping("/perfilutilizador/consulta/{idconsulta}/receita")
    public String verReceita(@PathVariable("idconsulta") int idconsulta, Model model) {
        model.addAttribute("mensagem", "ID Consulta" + idconsulta);

        model.addAttribute("idconsulta", idconsulta);

        List<Receita> r = Receita.getListaReceitas(idconsulta);

        model.addAttribute("listareceita", r);

        return "receita";
    }

    /**
     * Redireciona o utilizador para a pagina da fatura
     * @param idconsulta
     * @param model
     * @return
     */
    @RequestMapping("/perfilutilizador/consulta/{idconsulta}/fatura")
    public String verFatura(@PathVariable("idconsulta") int idconsulta, Model model) {
        model.addAttribute("mensagem", "ID Consulta" + idconsulta);

        model.addAttribute("idconsulta", idconsulta);

        Fatura f = Fatura.getFatura(idconsulta);

        model.addAttribute("fatura", f);

        return "fatura";
    }

    /**
     * Cancela uma determida consulta.
     * @param id
     * @param idconsulta
     * @param redirAttrs
     * @return
     */
    @RequestMapping("/perfilutilizador/{id}/cancelar/{idconsulta}")
    public String cancelarConsulta(@PathVariable("id") int id, @PathVariable("idconsulta") int idconsulta, RedirectAttributes redirAttrs) {

        int salaAtual = Consulta.getConsulta(idconsulta).getSala();

        if (Consulta.getConsulta(idconsulta).getEstado().equals("Confirmada")) {
            Consulta.alteraEstado(idconsulta, salaAtual, "Cancelada");
            System.out.println("Cancelada com sucesso!");

            redirAttrs.addFlashAttribute("success", "Consulta cancelada com sucesso!");
        } else {
            redirAttrs.addFlashAttribute("error", "Não pode cancelar esta consulta!");
        }


        return "redirect:/perfilutilizador/" + id;
    }


    /**
     * Redireciona para a página do perfil
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/perfilutilizador/perfil/{idutilizador}")
    public String verPerfil(@PathVariable("idutilizador") int id, Model model) {

        Utente u = Utente.getUtente(id);

        model.addAttribute("utente", u);

        return "perfil";
    }


    /**Redireciona para a view de editar perfil*/
    @RequestMapping("/perfilutilizador/perfil/{idutilizador}/editar")
    public String editarPerfil(@PathVariable("idutilizador") int id, Model model) {

        Utente u = Utente.getUtente(id);

        model.addAttribute("utente", u);

        return "editarPerfil";
    }



    /**
     * Altera os dados de um utilizador
     * @param id
     * @param dados
     * @return
     * */
    @PostMapping("/alteradados/{id}")
    public String alteraDados(@ModelAttribute("dados") Utente dados, @PathVariable("id") int id) {


        String contacto = dados.getContacto();
        String morada = dados.getMorada();

        Utente.alteraDados(id,morada,contacto);
        //Adicionar alerta entre páginas?

        return "redirect:/perfilutilizador/perfil/" + id;

    }

    /**Redireciona para a view de marcar Consulta*/
    @RequestMapping("/perfilutilizador/{idutilizador}/marcarconsulta")
    public String marcarView(@PathVariable("idutilizador") int id, Model model) {

        //Utente u = Utente.getUtente(id);

        model.addAttribute("idutilizador", id);

        List<Dentista> listaDentistas = Dentista.getListaDentistas();
        List<Procedimento> listaProceds = Procedimento.getListaProcedimentos();

        model.addAttribute("listadentistas", listaDentistas);
        model.addAttribute("listaproceds", listaProceds);

        return "marcarconsulta";
    }

    /**
     * Marcar Consulta
     * @param model
     * @param formData
     * @param id
     * @param d
     * @param p
     * @param redirAttrs
     * @return
     */
    @RequestMapping(value = "/perfilutilizador/marcacao/{idutilizador}", method = RequestMethod.POST)
    public String marcarConsulta(@ModelAttribute("consulta") Dentista d, Procedimento p,DateFormat formData, @PathVariable("idutilizador") int id, Model model, RedirectAttributes redirAttrs) {

        int idDentista = d.getIddent();
        int idProced = p.getIdproced();


        Date horaDate = formData.getHora();
        Date dataDate = formData.getData();


        SimpleDateFormat formatterHoras = new SimpleDateFormat("HH:mm");
        SimpleDateFormat formatterDia = new SimpleDateFormat("dd-MM-yyyy");


        String hora = formatterHoras.format(horaDate);
        String dataIni = formatterDia.format(dataDate);


        System.out.println(idDentista);
        System.out.println(idProced);
        System.out.println(hora);
        System.out.println(dataIni);
        //System.out.println(proced);
        

        List<Consulta> consultas = Consulta.getListaConsultas();
        

        List<Procedimento> proceds = Procedimento.getListaProcedimentos();
        
        int duracao = 0;
        
        for (Procedimento proc: proceds){
            if (proc.getIdproced()==idProced){
                duracao = proc.getDuracao();
            }
        }



        String str = dataIni + " " + hora;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        LocalDateTime dataHoraInicio = LocalDateTime.parse(str, formatter);
        System.out.println(dataHoraInicio);


        LocalDateTime dataHoraFim = dataHoraInicio.plusHours(duracao);
        System.out.println("Data Fim " + dataHoraFim);


        Boolean hourOccupied = false;
        List<Integer> salasOcupadas = new ArrayList<>();


        for (Consulta c : consultas) {

            if (dataHoraFim.isBefore(c.getDatafim()) && dataHoraFim.isAfter(c.getDatainicio())) {
                if (c.getIddent().getIddent()==idDentista) {
                    hourOccupied = true;
                    redirAttrs.addFlashAttribute("error", "Horário do doutor selecionado indisponível");
                    return "redirect:/perfilutilizador/" + id + "/marcarconsulta";
                } else {
                    salasOcupadas.add(c.getSala());
                }
            } else if (dataHoraInicio.isAfter(c.getDatainicio()) && dataHoraInicio.isBefore(c.getDatafim())) {
                if (c.getIddent().getIddent()==idDentista) {
                    hourOccupied = true;
                    redirAttrs.addFlashAttribute("error", "Horário do doutor selecionado indisponível");

                    return "redirect:/perfilutilizador/" + id + "/marcarconsulta";
                } else {
                    salasOcupadas.add(c.getSala());
                }
            } else if (dataHoraInicio.isAfter(c.getDatainicio()) && dataHoraFim.isBefore(c.getDatafim())) {
                if (c.getIddent().getIddent()==idDentista) {
                    hourOccupied = true;
                    redirAttrs.addFlashAttribute("error", "Horário do doutor selecionado indisponível");

                    return "redirect:/perfilutilizador/" + id + "/marcarconsulta";
                } else {
                    salasOcupadas.add(c.getSala());
                }
            } else if (dataHoraInicio.equals(c.getDatainicio())) {
                if (c.getIddent().getIddent()==idDentista) {
                    hourOccupied = true;
                    redirAttrs.addFlashAttribute("error", "Horário do doutor selecionado indisponível");

                    return "redirect:/perfilutilizador/" + id + "/marcarconsulta";
                } else {
                    salasOcupadas.add(c.getSala());
                }
            }
        }

        if (!hourOccupied) {
            Random rnd = new Random();
            int sala = getRandomRoom(rnd, 1, 12, salasOcupadas);
            Consulta.addConsulta(dataIni, hora, sala, idDentista, id, idProced);
            redirAttrs.addFlashAttribute("success", "Consulta Marcada com sucesso!");
        }


        return "redirect:/perfilutilizador/" + id + "/marcarconsulta";
    }


    /**
     * Função auxiliar para converter um Date em LocalDate.
     * @param dateToConvert
     * @return
     * */
    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    /**
     * Função auxiliar para atribuir uma sala à consulta.
     * @param start
     * @param end
     * @param exclude
     * @param exclude
     * @param rnd
     * @return
     * */
    public int getRandomRoom(Random rnd, int start, int end, List<Integer> exclude){
        int random = start + rnd.nextInt(end - start + 1 - exclude.size());
        for( int ex : exclude){
            if(random < ex){
                break;
            }
            random++;
        }
        return random;
    }

    public List<Consulta> getConsultasUtenteConfirmadas(List<Consulta> consultas){

        List<Consulta> consultasConfirmadas = new ArrayList<>();

        for (Consulta c: consultas){
            if (c.getEstado().equals("Confirmada")){
                consultasConfirmadas.add(c);
            }
        }

        return consultasConfirmadas;

    }

    public List<Consulta> getConsultasUtenteHoje(List<Consulta> consultas){

        List<Consulta> consultasHoje = new ArrayList<>();

        DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        String data;
        String today;

        for (Consulta c: consultas){
            data = formatterData.format(c.getDatainicio());
            today = formatterData.format(LocalDateTime.now());

            if (data.equals(today)){
                consultasHoje.add(c);
            }
        }
        return consultasHoje;

    }

    public List<Consulta> getConsultasUtenteHistorico(List<Consulta> consultas){

        List<Consulta> consultasHistorico = new ArrayList<>();

        for (Consulta c: consultas){

            LocalDateTime hoje = LocalDateTime.now();

            if (c.getDatainicio().isBefore(hoje)){
                consultasHistorico.add(c);
            }
        }
        return consultasHistorico;
    }

    public List<Consulta> getConsultasUtente(List<Consulta> consultas,int id){

        List<Consulta> consultasUtente = new ArrayList<>();

        for (Consulta c: consultas){


            if (c.getIdutente().getIdutente()==id){
                consultasUtente.add(c);
            }
        }
        return consultasUtente;
    }

    protected Optional<String> getPreviousPageByRequest(HttpServletRequest request)
    {
        return Optional.ofNullable(request.getHeader("Referer")).map(requestUrl -> "redirect:" + requestUrl);
    }

    /**
     * Obtém a lista de todos os procedimentos disponiveis.
     * @param model
     * @return
     * */
    @RequestMapping(value = "/perfilutilizador/{idutilizador}/procedimentos")
    public String pagProcedimentos(Model model,@PathVariable("idutilizador") int id){


        List<Procedimento> procedimentos = Procedimento.getListaProcedimentos();

        model.addAttribute("listaproced",procedimentos);
        model.addAttribute("idutilizador",id);

        return "procedimentosUtilizador";

    }


}